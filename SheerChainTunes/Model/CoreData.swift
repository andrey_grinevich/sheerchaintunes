//
//  CoreData.swift
//  SheerChainTunes
//
//  Created by Andrey Grinevich on 10/28/18.
//  Copyright © 2018 Andrey Grinevich. All rights reserved.
//

import UIKit
import CoreData

public class CoreData {
    
    func addToLocalStore() {
        DispatchQueue.main.async {
            for object in ArtistsDataFromAPI.artistItems {
                guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
                let managedContext = appDelegate.persistentContainer.viewContext
                let entity = NSEntityDescription.entity(forEntityName: "Artist", in: managedContext)!
                
                let item = NSManagedObject(entity: entity, insertInto: managedContext)
                item.setValue(object.artistName, forKey: "artistName")
                item.setValue(object.listenersCounter, forKey: "artistListeners")
                item.setValue(object.imageLink, forKey: "artistImageLink")
                
                do {
                    try managedContext.save()
                } catch let error as NSError {
                    print("Save error - \(error), \(error.userInfo.description)")
                }
            }
        }
    }
    
    func fetchFromCoreData() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Artist")
        do {
            var items: [NSManagedObject] = []
            items = try managedContext.fetch(fetchRequest)
            for index in 0..<items.count {
                let item = items[index]
                let newItem = ArtistItems.init(artistName: item.value(forKeyPath: "artistName") as? String ?? "Artist Name",
                                               imageLink: "Raccoon",
                                               listenersCounter: item.value(forKeyPath: "artistListeners") as? String ?? "xxx listeners",
                                               image: UIImage(named: "imageEmptyArtistContainer")!)
                if ArtistsDataFromAPI.artistItems.count < 50 {
                    ArtistsDataFromAPI.artistItems.append(newItem)
                }
            }
            NotificationCenter.default.post(Notification.init(name: Notification.Name(rawValue: "topArtistsDataDidReceived")))
        } catch let error as NSError {
            print("Save error - \(error), \(error.userInfo.description)")
        }
    }
    
    func deleteDataFromCoreData() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Artist")
        do {
            var item = try managedContext.fetch(fetchRequest)
            
            for index in 0..<item.count {
                let objectToDelete = item[index]
                managedContext.delete(objectToDelete)
            }
            do {
                try managedContext.save()
            } catch {
                print(error)
            }
        } catch let error as NSError {
            print("Save error - \(error), \(error.userInfo.description)")
        }
    }
}
