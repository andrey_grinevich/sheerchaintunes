//
//  TopAlbum.swift
//  SheerChainTunes
//
//  Created by Andrey Grinevich on 10/25/18.
//  Copyright © 2018 Andrey Grinevich. All rights reserved.
//

import UIKit

class AlbumsDataFromAPI {
    static var albumItems = [AlbumItems]()
}

struct AlbumItems {
    var albumName: String
    var imageLink: String
    var playcount: Int
}

struct TopAlbum: Codable {
    let topalbums: Topalbums
    
    struct Topalbums: Codable {
        let album: [Album]
        let attr: Attr
        
        enum CodingKeys: String, CodingKey {
            case album
            case attr = "@attr"
        }
        
        struct Album: Codable {
            let name: String
            let playcount: Int
            let url: String
            let artist: ArtistClass
            let image: [Image]
            
            struct ArtistClass: Codable {
                let name: String
                let mbid: String
                let url: String
            }
            
            struct Image: Codable {
                let text: String
                let size: Size
                
                enum CodingKeys: String, CodingKey {
                    case text = "#text"
                    case size
                }
            }
            
            enum Size: String, Codable {
                case extralarge = "extralarge"
                case large = "large"
                case medium = "medium"
                case small = "small"
            }
        }
   
        struct Attr: Codable {
            let artist: String
            let page, perPage, totalPages, total: String
        }
    }
}

