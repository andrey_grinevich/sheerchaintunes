//
//  Extension.swift
//  SheerChainTunes
//
//  Created by Andrey Grinevich on 10/27/18.
//  Copyright © 2018 Andrey Grinevich. All rights reserved.
//

import UIKit

extension UIImageView {
    func downloadedFrom(link:String) {
        guard let url = URL(string: link) else { return }
        URLSession.shared.dataTask(with: url, completionHandler: { (data, _, error) -> Void in
            guard let data = data , error == nil, let image = UIImage(data: data) else { return }
            DispatchQueue.main.async { () -> Void in
                self.image = image
            }
        }).resume()
    }
}

extension String{
    var encodeUrl: String { return self.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)! }
    var decodeUrl: String { return self.removingPercentEncoding! }
}

extension TopAtrtistsVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if ArtistsDataFromAPI.artistItems.count == 0 && Reachability.hasConnection == false {
            return 1
        } else {
            return ArtistsDataFromAPI.artistItems.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let topArtistCell = Bundle.main.loadNibNamed("TopArtistTableViewCell", owner: self, options: nil)?.first as! TopArtistTableViewCell
        if ArtistsDataFromAPI.artistItems.count == 0 && Reachability.hasConnection == false {
            topArtistCell.artistImageView.image = UIImage(named: "imageEmptyArtistContainer")
            topArtistCell.artistNameLabel.text = "Local storage is empty \nTurn on internet connection"
            topArtistCell.artistListenersLabel.text = "Offline mode"
        } else {
            topArtistCell.artistImageView.downloadedFrom(link: ArtistsDataFromAPI.artistItems[indexPath.row].imageLink)
            topArtistCell.artistNameLabel.text = ArtistsDataFromAPI.artistItems[indexPath.row].artistName
            topArtistCell.artistListenersLabel.text = ArtistsDataFromAPI.artistItems[indexPath.row].listenersCounter + " listeners"
        }
        return topArtistCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let selectedArtistVC = storyboard.instantiateViewController(withIdentifier: "SelectedArtistVC") as! SelectedArtistVC
        if ArtistsDataFromAPI.artistItems.count != 0 {
            selectedArtistVC.selectedArtist = ArtistsDataFromAPI.artistItems[indexPath.row].artistName
        }
        self.navigationController?.pushViewController(selectedArtistVC, animated: true)
    }
}

extension SelectedArtistVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if AlbumsDataFromAPI.albumItems.count == 0 && Reachability.hasConnection == false {
            return 1
        } else {
            return AlbumsDataFromAPI.albumItems.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let topAlbumCell = Bundle.main.loadNibNamed("TopAlbumTableViewCell", owner: self, options: nil)?.first as! TopAlbumTableViewCell
        if AlbumsDataFromAPI.albumItems.count == 0 && Reachability.hasConnection == false {
            topAlbumCell.topAlbumImageView.image = UIImage(named: "imageEmptyAlbumContainer")
            topAlbumCell.topAlbumLabel.text = "Offline mode"
            topAlbumCell.albumPlaycounterLabel.text = "play count = ∞"
        } else {
            topAlbumCell.topAlbumImageView.downloadedFrom(link: AlbumsDataFromAPI.albumItems[indexPath.row].imageLink)
            topAlbumCell.topAlbumLabel.text = AlbumsDataFromAPI.albumItems[indexPath.row].albumName
            topAlbumCell.albumPlaycounterLabel.text = "play count = " + AlbumsDataFromAPI.albumItems[indexPath.row].playcount.description
        }
        return topAlbumCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let selectedAlbumVC = storyboard.instantiateViewController(withIdentifier: "SelectedAlbumVC") as! SelectedAlbumVC
        if AlbumsDataFromAPI.albumItems.count != 0 {
            selectedAlbumVC.selectedAlbumName = AlbumsDataFromAPI.albumItems[indexPath.row].albumName
            selectedAlbumVC.selectedAlbumImageLink = AlbumsDataFromAPI.albumItems[indexPath.row].imageLink
        }
        self.navigationController?.pushViewController(selectedAlbumVC, animated: true)
    }
}

extension SelectedAlbumVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return songs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let selectedAlbumCell = Bundle.main.loadNibNamed("SelectedAlbumTableViewCell", owner: self, options: nil)?.first as! SelectedAlbumTableViewCell
        selectedAlbumCell.songLabel.text = songs[indexPath.row]
        return selectedAlbumCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}
