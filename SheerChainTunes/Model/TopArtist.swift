//
//  TopArtists.swift
//  SheerChainTunes
//
//  Created by Andrey Grinevich on 10/25/18.
//  Copyright © 2018 Andrey Grinevich. All rights reserved.
//

import UIKit

class ArtistsDataFromAPI {
    
    static var artistItems = [ArtistItems]()
    let TopArtistsDataDidReceived = "TopArtistsDataDidReceived"
    
    func getTopArtistsDataFromAPI(jsonUrlString: String) {
        
        guard let url = URL(string: jsonUrlString) else { return }
        
        URLSession.shared.dataTask(with: url) { (optionalData, response, jsonError) in
            guard let data = optionalData else { return }
            do {
                let artistData = try JSONDecoder().decode(TopArtist.self, from: data)
                ArtistsDataFromAPI.artistItems.removeAll()
                for index in 0..<50 {
                    let newItem = ArtistItems.init(artistName: artistData.topartists.artist[index].name,
                                                   imageLink: artistData.topartists.artist[index].image[1].text,
                                                   listenersCounter: artistData.topartists.artist[index].listeners,
                                                   image: UIImage(named: "imageEmptyArtistContainer")!)
                    ArtistsDataFromAPI.artistItems.append(newItem)
                }
                ArtistsDataFromAPI.artistItems.sort {$0.artistName < $1.artistName}
                CoreData().addToLocalStore()
                NotificationCenter.default.post(Notification.init(name: Notification.Name(rawValue: self.TopArtistsDataDidReceived)))
            } catch let jsonError {
                print("Error", jsonError)
            }
        }.resume()
    }
}

struct ArtistItems {
    var artistName: String
    var imageLink: String
    var listenersCounter: String
    var image: UIImage
}

struct TopArtist: Codable {
    
    enum Request: String {
        case forUkraine = "http://ws.audioscrobbler.com/2.0/?method=geo.gettopartists&country=ukraine&api_key=e81f61890b7ff8633ca024d0faa449e7&format=json"
        case forGeorgia = "http://ws.audioscrobbler.com/2.0/?method=geo.gettopartists&country=georgia&api_key=e81f61890b7ff8633ca024d0faa449e7&format=json"
        case forGermany = "http://ws.audioscrobbler.com/2.0/?method=geo.gettopartists&country=germany&api_key=e81f61890b7ff8633ca024d0faa449e7&format=json"
    }
    
    let topartists: Topartists
    
    struct Topartists: Codable {
        let artist: [Artist]
        let attr: Attr
        
        struct Artist: Codable {
            let name, listeners, mbid: String
            let url: String
            let streamable: String
            let image: [Image]
            
            struct Image: Codable {
                let text: String
                let size: Size
                
                enum CodingKeys: String, CodingKey {
                    case text = "#text"
                    case size
                }
                
                enum Size: String, Codable {
                    case extralarge = "extralarge"
                    case large = "large"
                    case medium = "medium"
                    case mega = "mega"
                    case small = "small"
                }
            }
        }
        
        struct Attr: Codable {
            let country, page, perPage, totalPages: String
            let total: String
        }
        
        enum CodingKeys: String, CodingKey {
            case artist
            case attr = "@attr"
        }
    }
}
