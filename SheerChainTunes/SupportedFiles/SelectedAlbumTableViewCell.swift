//
//  SelectedAlbumTableViewCell.swift
//  SheerChainTunes
//
//  Created by Andrey Grinevich on 10/28/18.
//  Copyright © 2018 Andrey Grinevich. All rights reserved.
//

import UIKit

class SelectedAlbumTableViewCell: UITableViewCell {

    @IBOutlet weak var songLabel: UILabel!

}
