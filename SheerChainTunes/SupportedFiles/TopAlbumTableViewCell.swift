//
//  TopAlbumTableViewCell.swift
//  SheerChainTunes
//
//  Created by Andrey Grinevich on 10/26/18.
//  Copyright © 2018 Andrey Grinevich. All rights reserved.
//

import UIKit

class TopAlbumTableViewCell: UITableViewCell {

    @IBOutlet weak var topAlbumImageView: UIImageView!
    @IBOutlet weak var topAlbumLabel: UILabel!
    @IBOutlet weak var albumPlaycounterLabel: UILabel!
    
}
