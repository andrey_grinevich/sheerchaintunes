//
//  TopArtistTableViewCell.swift
//  SheerChainTunes
//
//  Created by Andrey Grinevich on 10/25/18.
//  Copyright © 2018 Andrey Grinevich. All rights reserved.
//

import UIKit

class TopArtistTableViewCell: UITableViewCell {
    
    @IBOutlet weak var artistImageView: UIImageView!
    @IBOutlet weak var artistNameLabel: UILabel!
    @IBOutlet weak var artistListenersLabel: UILabel!
    
}
