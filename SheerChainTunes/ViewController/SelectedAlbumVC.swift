//
//  SelectedAlbumVC.swift
//  SheerChainTunes
//
//  Created by Andrey Grinevich on 10/25/18.
//  Copyright © 2018 Andrey Grinevich. All rights reserved.
//

import UIKit

class SelectedAlbumVC: UIViewController {

    @IBOutlet weak var selectedAlbumImageView: UIImageView!
    @IBOutlet weak var selectedAlbumItemsTableView: UITableView!
    
    var selectedAlbumName = "Album"
    var selectedAlbumImageLink = ""
    let songs = ["1. First Song (4:42)", "2. Second Song (2:47)", "3. Third Song (5:12)", "4. Fourth Song (4:38)", "5. Fifth Song (3:11)"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = selectedAlbumName
        selectedAlbumImageView.image = UIImage(named: "imageEmptyAlbumContainer")
        selectedAlbumImageView.downloadedFrom(link: selectedAlbumImageLink)
    }
}
