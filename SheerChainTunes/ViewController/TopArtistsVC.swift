//
//  TopArtistsVC.swift
//  SheerChainTunes
//
//  Created by Andrey Grinevich on 10/25/18.
//  Copyright © 2018 Andrey Grinevich. All rights reserved.
//

import UIKit

class TopAtrtistsVC: UIViewController {
    
    @IBOutlet weak var topArtistTableView: UITableView!
    @IBOutlet weak var selectedCountryLabel: UILabel!
    @IBOutlet weak var selectedCountryImageView: UIImageView!
    @IBOutlet weak var previousCountryButton: UIButton!
    @IBOutlet weak var nextCountryButton: UIButton!
    @IBOutlet weak var offlineLabelImageView: UIImageView!
    
    var indexSelectedCountry = 0    // Index of current selected country for request
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateTableView(notification:)), name: NSNotification.Name(rawValue: ArtistsDataFromAPI().TopArtistsDataDidReceived), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(networkStatusChanged(_:)), name: NSNotification.Name(rawValue: ReachabilityStatusChangedNotification), object: nil)
        
        Reachability().monitorReachabilityChanges()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        clearAlbumData()
    }
    
    @objc func networkStatusChanged(_ notification: Notification) {
        let userInfo = (notification as NSNotification).userInfo
        if userInfo!.description.hasPrefix("[AnyHashable(\"Status\"): \"Online") {
            offlineLabelImageView.alpha = 0
            Reachability.hasConnection = true
            CoreData().deleteDataFromCoreData()
            ArtistsDataFromAPI().getTopArtistsDataFromAPI(jsonUrlString: TopArtist.Request.forUkraine.rawValue)
        } else {
            offlineLabelImageView.alpha = 1
            Reachability.hasConnection = false
            CoreData().fetchFromCoreData()
        }
    }
    
    @objc func updateTableView(notification: Notification) {
        DispatchQueue.main.async {
            self.topArtistTableView.reloadData()
        }
    }
    
    func updateArtistDataFromAPI() {
        switch indexSelectedCountry {
        case 0:
            clearArtistData()
            ArtistsDataFromAPI().getTopArtistsDataFromAPI(jsonUrlString: TopArtist.Request.forUkraine.rawValue)
            selectedCountryLabel.text = "Ukraine"
            selectedCountryImageView.image = UIImage(named: "imageUkraineLocation")
            previousCountryButton.alpha = 0.3
        case 1:
            clearArtistData()
            ArtistsDataFromAPI().getTopArtistsDataFromAPI(jsonUrlString: TopArtist.Request.forGeorgia.rawValue)
            selectedCountryLabel.text = "Georgia"
            selectedCountryImageView.image = UIImage(named: "imageGeorgiaLocation")
            previousCountryButton.alpha = 1
            nextCountryButton.alpha = 1
        case 2:
            clearArtistData()
            ArtistsDataFromAPI().getTopArtistsDataFromAPI(jsonUrlString: TopArtist.Request.forGermany.rawValue)
            selectedCountryLabel.text = "Germany"
            selectedCountryImageView.image = UIImage(named: "imageGermanyLocation")
            nextCountryButton.alpha = 0.3
        default:
            print("index of selected Country out of range")
        }
    }
    
    func clearAlbumData() {
        for index in 0..<AlbumsDataFromAPI.albumItems.count {
            AlbumsDataFromAPI.albumItems[index].imageLink = ""
        }
    }
    
    func clearArtistData() {
        for index in 0..<ArtistsDataFromAPI.artistItems.count {
            ArtistsDataFromAPI.artistItems[index].imageLink = ""
        }
    }
    
    @IBAction func previousCountryButton(_ sender: UIButton) {
        if indexSelectedCountry != 0 {
            indexSelectedCountry -= 1
            updateArtistDataFromAPI()
        }
    }
    
    @IBAction func nextCountryButton(_ sender: UIButton) {
        if indexSelectedCountry != 2 {
            indexSelectedCountry += 1
            updateArtistDataFromAPI()
        }
    }
}
