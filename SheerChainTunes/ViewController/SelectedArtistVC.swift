//
//  SelectedArtistVC.swift
//  SheerChainTunes
//
//  Created by Andrey Grinevich on 10/25/18.
//  Copyright © 2018 Andrey Grinevich. All rights reserved.
//

import UIKit

class SelectedArtistVC: UIViewController {

    @IBOutlet weak var topAlbumTableView: UITableView!

    var selectedArtist = "Artist"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(updateTableView(notification:)), name: NSNotification.Name(rawValue: "topAlbumsDataDidReceived"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = selectedArtist
        getTopAlbumsData(selectedArtist: selectedArtist)
    }
    
    @objc func updateTableView(notification: Notification) {
        DispatchQueue.main.async {
            self.topAlbumTableView.reloadData()
        }
    }
    
    func getTopAlbumsData(selectedArtist: String) {
        let jsonUrlString = "http://ws.audioscrobbler.com/2.0/?method=artist.gettopalbums&artist=" + selectedArtist + "&api_key=e81f61890b7ff8633ca024d0faa449e7&format=json"
        guard let url = URL(string: jsonUrlString.encodeUrl) else { return }
        URLSession.shared.dataTask(with: url) { (optionalData, response, jsonError) in
            guard let data = optionalData else { return }
            AlbumsDataFromAPI.albumItems.removeAll()
            do {
                let albumData = try JSONDecoder().decode(TopAlbum.self, from: data)
                for index in 0..<50 {
                    let newItem = AlbumItems.init(albumName: albumData.topalbums.album[index].name,
                                                   imageLink: albumData.topalbums.album[index].image[1].text,
                                                   playcount: albumData.topalbums.album[index].playcount)
                    if newItem.albumName != "(null)" {
                        AlbumsDataFromAPI.albumItems.append(newItem)
                    }
                }
                NotificationCenter.default.post(Notification.init(name: Notification.Name(rawValue: "topAlbumsDataDidReceived")))
            } catch let jsonError {
                print("Error code: ", jsonError)
            }
        }.resume()
    }
}


